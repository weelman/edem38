@extends('layouts.mainlayout')
   @section('title', 'Гостиничный комплекс Эдем')
   @section('beforemenu')
            <video autoplay muted poster="/images/bg-head-min.jpg" id="bgvid" loop>
                <source src="/images/backgroundvideo.webm" type="video/webm">
                <source src="/images/backgroundvideo.mp4" type="video/mp4">
            </video>
            @stop
            
    @section('aftermenu')  
        <div class="head-text" style="margin-top:15%;">
            <h2>Эко-отель</h2>
            <p>ЭДЕМ</p>
            <a href="http://edem38ru-book.otelms.com/bookit/step1">ЗАБРОНИРОВАТЬ</a>
        </div>
       <div class="mainpage">
        <div class="container">
            <div class="main-blocks">
                <div class="content-block col-md-5 col-sm-12" id="main-img-1">
                    <img src="/images/IMG_1347.JPG" alt="">
                </div>
                <div class="content-block col-md-7 col-sm-12" id="main-text-1">
                    <center><img src="/images/wv1.png" alt=""></center>
                    <h4>О НАС</h4>
                    <p>Коттедж посуточно можно снять в Листвянке для отдыха и проведения различных мероприятий. В наших комфортабельных аппартаментах Вы сможете с удовольствием отдохнуть в одном их красивейших мест планеты.</p>
                    <p>На Байкале много прекрасных мест, таинственных и удивительных, как и само озеро. Но преимущество гостиницы в Листвянке заключается в том, что она является ближайшей точкой, расположенной от областного центра.</p>
                    <p>Семейный отдых на Байкале все больше привлекает внимание людей, и неспроста, ведь отдых с детьми - это незабываемое впечатление как для родителей, так и для ребенка.</p>
                    <p>Для отдыхающих здесь созданы все условия, чтобы вы могли чувствовать себя комфортно как в городе, с одной стороны, а с другой – каждый, кто стремится приехать на Байкал, хочет прикоснуться к самой природе, пожить вдали от городского шума, словно вы находитесь на турбазе, попариться в бане, подышать свежим воздухом, посидеть у костра.</p>
                </div>
                <div class="content-block col-sm-12" id="main-img-2">
                    <img src="/images/dsc_5178-pano.jpg" alt="">
                </div>
            </div>
        </div>
        </div>  
    <div class="col-sm-12" id="whereweare">
        <h3>ГДЕ НАС НАЙТИ?</h3>
    </div>
    
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2463.9534427666968!2d104.87627561578303!3d51.86180647969502!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x97537a5955e5e807!2z0K3QtNC10LwsINCz0L7RgdGC0LXQstC-0Lkg0LTQvtC8!5e0!3m2!1sru!2s!4v1467644878090" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
    
    
<!--    <a class="dg-widget-link" href="http://2gis.ru/irkutsk/firm/1548641653311083/center/104.87847,51.862092/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Иркутска</a><div class="dg-widget-link"><a href="http://2gis.ru/irkutsk/firm/1548641653311083/photos/1548641653311083/center/104.87847,51.862092/zoom/17?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=photos">Фотографии компании</a></div><div class="dg-widget-link"><a href="http://2gis.ru/irkutsk/center/104.87847,51.862092/zoom/16/routeTab/rsType/bus/to/104.87847,51.862092╎Эдем, гостевой дом?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Эдем, гостевой дом</a></div><script charset="utf-8" src="http://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":'100%',"height":600,"borderColor":"#a3a3a3","pos":{"lat":51.862092,"lon":104.87847,"zoom":16},"opt":{"city":"irkutsk"},"org":[{"id":"1548641653311083"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>-->
    
    @stop
    