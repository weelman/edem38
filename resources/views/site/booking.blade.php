<!DOCTYPE html>
<html class="html" lang="ru-RU">
 <head>

  <script type="text/javascript">
   if(typeof Muse == "undefined") window.Muse = {}; window.Muse.assets = {"required":["jquery-1.8.3.min.js", "museutils.js", "jquery.musepolyfill.bgsize.js", "jquery.watch.js", "bronirovanie.css"], "outOfDate":[]};
</script>
  
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <meta name="generator" content="2015.0.2.310"/>
  <title>Бронирование</title>
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="css/site_global.css?4052507572"/>
  <link rel="stylesheet" type="text/css" href="css/bronirovanie.css?3798019405" id="pagesheet"/>
  <!--[if lt IE 9]>
  <link rel="stylesheet" type="text/css" href="css/iefonts_bronirovanie.css?4164407343"/>
  <![endif]-->
  <!-- Other scripts -->
  <script type="text/javascript">
   document.documentElement.className += ' js';
var __adobewebfontsappname__ = "muse";
</script>
  <!-- JS includes -->
  <script type="text/javascript">
   document.write('\x3Cscript src="' + (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//webfonts.creativecloud.com/roboto:n4,n3,n7,n5:all.js" type="text/javascript">\x3C/script>');
</script>
   </head>
 <body>

  <div class="clearfix" id="page"><!-- column -->
   <div class="position_content" id="page_position_content">
    <div class="clearfix colelem" id="pu1144"><!-- group -->
     <div class="browser_width grpelem" id="u1144-bw">
      <div class="museBGSize" id="u1144"><!-- simple frame --></div>
     </div>
     <div class="browser_width grpelem" id="u1145-bw">
      <div class="rgba-background" id="u1145"><!-- group -->
       <div class="clearfix" id="u1145_align_to_page">
        <div class="clearfix grpelem" id="u1174-4"><!-- content -->
         <p>Бронирование</p>
        </div>
       </div>
      </div>
     </div>
     <div class="browser_width grpelem" id="u1146-bw">
      <div id="u1146"><!-- simple frame --></div>
     </div>
     <a class="nonblock nontext clip_frame grpelem" id="u1147" href="{{ URL::route('index')}}"><!-- image --><img class="block" id="u1147_img" src="images/logo.png" alt="" width="103" height="43"/></a>
     <a class="nonblock nontext clearfix grpelem" id="u1149-4" href="{{ URL::route('price')}}"><!-- content --><h2>ЦЕНЫ</h2></a>
     <div class="clearfix grpelem" id="u1150-4"><!-- content -->
      <h2>БРОНИРОВАНИЕ</h2>
     </div>
     <a class="nonblock nontext clearfix grpelem" id="u1151-4" href="{{ URL::route('contacts')}}"><!-- content --><h2>КОНТАКТЫ</h2></a>
     <a class="nonblock nontext clearfix grpelem" id="u1152-4" href="{{ URL::route('about')}}"><!-- content --><h2>О НАС</h2></a>
     <a class="nonblock nontext clearfix grpelem" id="u1153-4" href="{{ URL::route('gallery')}}"><!-- content --><h2>ГАЛЕРЕЯ</h2></a>
     <a class="nonblock nontext clearfix grpelem" id="u1154-4" href="{{ URL::route('visa')}}"><!-- content --><h2>ВИЗА</h2></a>
     <a class="nonblock nontext clearfix grpelem" id="u1155-4" href="{{ URL::route('feedback')}}"><!-- content --><h2>ОТЗЫВЫ</h2></a>
     <div class="clearfix grpelem" id="u1156-4"><!-- content -->
      <p>LANGUAGE</p>
     </div>
    </div>
    <div class="clearfix colelem" id="pu1143"><!-- group -->
     <div class="clearfix grpelem" id="u1143"><!-- group -->
      <div class="clip_frame grpelem" id="u1368"><!-- image -->
       <img class="block" id="u1368_img" src="images/%d0%b2%d1%81%d1%82%d0%b0%d0%b2%d0%bb%d0%b5%d0%bd%d0%bd%d0%be%d0%b5%20%d0%b8%d0%b7%d0%be%d0%b1%d1%80%d0%b0%d0%b6%d0%b5%d0%bd%d0%b8%d0%b5%20778x657.jpg" alt="" width="778" height="657"/>
      </div>
     </div>
     <a class="nonblock nontext MuseLinkActive clearfix grpelem" id="u1374-4" href="http://edem38ru-book.otelms.com/bookit/step1"><!-- content --><p>ЗАБРОНИРОВАТЬ</p></a>
     <a class="nonblock nontext MuseLinkActive clearfix grpelem" id="u1375-4" href="http://edem38ru-book.otelms.com/bookit/step1"><!-- content --><p>ПРОВЕРИТЬ НАЛИЧИЕ МЕСТ</p></a>
    </div>
    <div class="verticalspacer"></div>
    <div class="clearfix colelem" id="pu1142"><!-- group -->
     <div class="browser_width grpelem" id="u1142-bw">
      <div id="u1142"><!-- simple frame --></div>
     </div>
     <div class="browser_width grpelem" id="u1157-bw">
      <div id="u1157"><!-- group -->
       <div class="clearfix" id="u1157_align_to_page">
        <div class="clearfix grpelem" id="pu1160"><!-- column -->
         <div class="clip_frame colelem" id="u1160"><!-- image -->
          <img class="block" id="u1160_img" src="images/phone.png" alt="" width="22" height="22"/>
         </div>
         <div class="clip_frame colelem" id="u1162"><!-- image -->
          <img class="block" id="u1162_img" src="images/point.png" alt="" width="14" height="23"/>
         </div>
        </div>
        <div class="clearfix grpelem" id="pu1165-4"><!-- column -->
         <div class="clearfix colelem" id="u1165-4"><!-- content -->
          <p>8 (3952) 73-40-98</p>
         </div>
         <div class="clearfix colelem" id="u1164-4"><!-- content -->
          <p>пос. Листвянка, ул. Чапаева, 98</p>
         </div>
        </div>
        <div class="clip_frame grpelem" id="u1158"><!-- image -->
         <img class="block" id="u1158_img" src="images/oplata.png" alt="" width="168" height="30"/>
        </div>
        <a class="nonblock nontext clip_frame grpelem" id="u1170" href="http://www.odnoklassniki.ru/profile/562900251596"><!-- image --><img class="block" id="u1170_img" src="images/ok.png" alt="" width="29" height="29"/></a>
        <a class="nonblock nontext clip_frame grpelem" id="u1168" href="http://www.facebook.com/profile.php?id=100009140466888"><!-- image --><img class="block" id="u1168_img" src="images/facebook.png" alt="" width="28" height="28"/></a>
        <a class="nonblock nontext clip_frame clearfix grpelem" id="u1166" href="http://vk.com/id232862672"><!-- image --><div id="u1166_clip"><img class="position_content" id="u1166_img" src="images/mezhdunarodny.png" alt="" width="30" height="30"/></div></a>
        <a class="nonblock nontext clip_frame grpelem" id="u1172" href="http://instagram.com/edem_baikal"><!-- image --><img class="block" id="u1172_img" src="images/insta.png" alt="" width="28" height="28"/></a>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
  <!-- JS includes -->
  <script type="text/javascript">
   if (document.location.protocol != 'https:') document.write('\x3Cscript src="http://musecdn.businesscatalyst.com/scripts/4.0/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
  <script type="text/javascript">
   window.jQuery || document.write('\x3Cscript src="scripts/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
  <script src="scripts/museutils.js?275725342" type="text/javascript"></script>
  <script src="scripts/jquery.musepolyfill.bgsize.js?185257658" type="text/javascript"></script>
  <script src="scripts/jquery.watch.js?3999102769" type="text/javascript"></script>
  <!-- Other scripts -->
  <script type="text/javascript">
   $(document).ready(function() { try {
(function(){var a={},b=function(a){if(a.match(/^rgb/))return a=a.replace(/\s+/g,"").match(/([\d\,]+)/gi)[0].split(","),(parseInt(a[0])<<16)+(parseInt(a[1])<<8)+parseInt(a[2]);if(a.match(/^\#/))return parseInt(a.substr(1),16);return 0};(function(){$('link[type="text/css"]').each(function(){var b=($(this).attr("href")||"").match(/\/?css\/([\w\-]+\.css)\?(\d+)/);b&&b[1]&&b[2]&&(a[b[1]]=b[2])})})();(function(){$("body").append('<div class="version" style="display:none; width:1px; height:1px;"></div>');
for(var c=$(".version"),d=0;d<Muse.assets.required.length;){var f=Muse.assets.required[d],g=f.match(/([\w\-\.]+)\.(\w+)$/),k=g&&g[1]?g[1]:null,g=g&&g[2]?g[2]:null;switch(g.toLowerCase()){case "css":k=k.replace(/\W/gi,"_").replace(/^([^a-z])/gi,"_$1");c.addClass(k);var g=b(c.css("color")),h=b(c.css("background-color"));g!=0||h!=0?(Muse.assets.required.splice(d,1),"undefined"!=typeof a[f]&&(g!=a[f]>>>24||h!=(a[f]&16777215))&&Muse.assets.outOfDate.push(f)):d++;c.removeClass(k);break;case "js":k.match(/^jquery-[\d\.]+/gi)&&
typeof $!="undefined"?Muse.assets.required.splice(d,1):d++;break;default:throw Error("Unsupported file type: "+g);}}c.remove();if(Muse.assets.outOfDate.length||Muse.assets.required.length)c="Некоторые файлы на сервере могут отсутствовать или быть некорректными. Очистите кэш-память браузера и повторите попытку. Если проблему не удается устранить, свяжитесь с разработчиками сайта.",(d=location&&location.search&&location.search.match&&location.search.match(/muse_debug/gi))&&Muse.assets.outOfDate.length&&(c+="\nOut of date: "+Muse.assets.outOfDate.join(",")),d&&Muse.assets.required.length&&(c+="\nMissing: "+Muse.assets.required.join(",")),alert(c)})()})();
/* body */
Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
Muse.Utils.prepHyperlinks(true);/* body */
Muse.Utils.resizeHeight()/* resize height */
Muse.Utils.fullPage('#page');/* 100% height page */
Muse.Utils.showWidgetsWhenReady();/* body */
Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
} catch(e) { if (e && 'function' == typeof e.notify) e.notify(); else Muse.Assert.fail('Error calling selector function:' + e); }});
</script>
   </body>
</html>
