@extends('layouts.mainlayout') 
@section('title', 'Место расположения гостиничного комплекса Edem')
@section('meta')
    @include('meta.contacts-meta')
@stop
@section('css')
<link rel="stylesheet" href="/css/forcontacts.css"> @stop @section('beforemenu')
<div class="background-div">
    <div class="blackout-div">
        @stop @section('aftermenu')
        <div class="head-text">
            <h3>Контакты</h3>
        </div>
    </div>
</div>
<div class="container">
    <div class="contacts-div col-sm-12">
        <p>Наш адрес:</p>
        <span>пос. Листвянка, ул. Чапаева, 104а</span>
        <p>Наш телефон:</p>
        <span>8 (3952) 73-40-98</span>
        <p>E-mail:</p>
        <span><a href="mailto:edem38@mail.ru">edem38@mail.ru</a></span>
    </div>
    <div class="question-block col-sm-12">
        <div class="col-sm-12">
            <p>Задать вопрос:</p>
        </div>
        <div class="col-sm-12 contacts-form-div">
            <form>
                <div class="form-group col-sm-6">
                    <label for="exampleInputName">Имя:</label>
                    <input type="text" class="form-control" id="exampleInputName" placeholder="Введите имя">
                </div>
                <div class="form-group col-sm-6">
                    <label for="exampleInputEmail">Электронная почта:</label>
                    <input type="email" class="form-control" id="exampleInputEmail" placeholder="Введите адрес электронной почты">
                </div>
                <div class="form-group col-sm-12">
                   <label for="exampleInputMessage">Сообщение:</label>
                    <textarea class="form-control" id="exampleInputMessage" rows="3">Введите сообщение</textarea>
                </div>
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-default">Отправить</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-sm-12">
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=6_5NMnoZpBCAnT3470Dq5QBC9h3rpfiC&width=100%&height=400&lang=ru_RU&sourceType=constructor&scroll=true"></script>
    </div>

</div>
@stop @section('scripts') @stop