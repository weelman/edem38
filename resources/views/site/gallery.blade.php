@extends('layouts.mainlayout')
@section('meta')
    @include('meta.gallery-meta')
@stop
   @section('title', 'Галерея фотографий гостиничного комплекса Edem')
   @section('css')
    <link href="assets/swipebox/swipebox.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    @stop
@section('beforemenu')
        <div class="background-div">
            <div class="blackout-div">
               @stop
                @section('aftermenu')
                <div class="head-text">
                    <h3>Галерея</h3>
                </div>
            </div>
        </div>

    <div id="loading"></div>
    <div id="gallery"></div>

   @stop

@section('scripts')
    <script type="text/javascript">
        ;( function( $ ) {

            $( '.swipebox' ).swipebox();

        } )( jQuery );
    </script>
    <script src="assets/swipebox/jquery.swipebox.min.js"></script>
    <script src="assets/js/jquery.loadImage.js"></script>
    <script src="assets/js/script.js"></script>
    @stop