<!DOCTYPE html>
<html class="html" lang="ru-RU">
 <head>

  <script type="text/javascript">
   if(typeof Muse == "undefined") window.Muse = {}; window.Muse.assets = {"required":["jquery-1.8.3.min.js", "museutils.js", "jquery.musepolyfill.bgsize.js", "jquery.watch.js", "price.css"], "outOfDate":[]};
</script>
  
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <meta name="generator" content="2015.0.2.310"/>
  <title>Цены</title>
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="css/site_global.css?4052507572"/>
  <link rel="stylesheet" type="text/css" href="css/price.css?4095679772" id="pagesheet"/>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!--[if lt IE 9]>
  <link rel="stylesheet" type="text/css" href="css/iefonts_price.css?37442396"/>
  <![endif]-->
  <!-- Other scripts -->
  <script type="text/javascript">
   document.documentElement.className += ' js';
var __adobewebfontsappname__ = "muse";
</script>
  <!-- JS includes -->
  <script type="text/javascript">
   document.write('\x3Cscript src="' + (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//webfonts.creativecloud.com/roboto:n4,n3,n5,n7:all.js" type="text/javascript">\x3C/script>');
</script>
   </head>
 <body>

  <div class="clearfix" id="page"><!-- column -->
   <div class="position_content" id="page_position_content">
    <div style="background-image:url('images/bg-head.jpg'); height: 300px;z-index:3;">
       <div style="height: 300px;background-color:rgba(0,0,0,0.4);z-index: 4;">
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <img class="block" id="u1106_img" src="images/logo.png" alt="" style="width:103px; height:43px; margin-top:3px; margin-left: 10px;">
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="#">ЦЕНЫ</a></li>
                        <li><a href="#" style="padding-right:6px;">БРОНИРОВАНИЕ</a></li>
                        <li><a href="#">О НАС</a></li>
                        <li><a href="#">ГАЛЕРЕЯ</a></li>
                        <li><a href="#" style="padding-right:7px">ВИЗА</a></li>
                        <li><a href="#" style="padding-left: 17px;">КОНТАКТЫ</a></li>
                        <li><a href="#" style="padding-left: 17px;">ОТЗЫВЫ</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Link</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
        <h1 style="text-align:center;font-size:40px;color:#FFFFFF;letter-spacing:2px;line-height:48px;font-family:roboto,sans-serif;font-weight:400;margin-top:89px;">Галерея</h1>
    </div>
    </div>
    <div class="clearfix colelem" id="pu1184"><!-- group -->
     <div class="clearfix grpelem" id="u1184"><!-- column -->
      <div class="position_content" id="u1184_position_content">
       <div class="colelem" id="pu1225"><!-- inclusion -->
        <div id="u1225"><!-- simple frame --></div>
        <div class="clearfix" id="pu1224-4"><!-- group -->
         <div class="clearfix grpelem" id="u1224-4"><!-- content -->
          <p>Коттедж на 20 человек</p>
         </div>
        </div>
       </div>
       <div class="clearfix colelem" id="pu1226"><!-- group -->
        <div class="clip_frame grpelem" id="u1226"><!-- image -->
         <img class="block" id="u1226_img" src="images/wxpdtm-hflk-crop-u1226.jpg" alt="" width="338" height="389"/>
        </div>
        <div class="clearfix grpelem" id="ppu1232"><!-- column -->
         <div class="colelem" id="pu1232"><!-- inclusion -->
          <div id="u1232"><!-- simple frame --></div>
          <div class="clearfix" id="pu1233-6"><!-- column -->
           <div class="clearfix colelem" id="u1233-6"><!-- content -->
            <p>Коттедж на 20 человек, включая три доп. места.</p>
            <p>Расчетный час: время заезда после 14:00, время выезда до 12:00.</p>
           </div>
           <div class="colelem" id="u1256"><!-- simple frame --></div>
           <div class="clearfix colelem" id="u1257-10"><!-- content -->
            <p id="u1257-3">В номере: <span id="u1257-2">банкетный зал на 20 человек,потолок 7м. DVD,Т.В., Караоке,WI-FI, кухня,со всей необходимой техникой: холодильник,свч-печь,стекло-керамическая плита, посуда, и. т. д. 7 комнат, 6 спален,2 санузла, душевые кабины.В номерах шкаф, туалетный столик, прикроватные тумбочки, дополнительный свет у изголовья кровати, WI-FI,спутниковое Т.В, с плоским экраном,балкон.</span></p>
            <p id="u1257-5">Номер может быть по желанию: DBL,TWN.</p>
            <p id="u1257-7">Дети до 10 лет бесплатно. (без отдельной кровати)</p>
            <p id="u1257-8">&nbsp;</p>
           </div>
          </div>
         </div>
         <div class="clearfix colelem" id="pu1234-4"><!-- group -->
          <div class="clearfix grpelem" id="u1234-4"><!-- content -->
           <p>Цена:</p>
          </div>
          <a class="nonblock nontext rounded-corners clearfix grpelem" id="u1236-4" href="bronirovanie.html"><!-- content --><p>ЗАБРОНИРОВАТЬ</p></a>
         </div>
        </div>
       </div>
      </div>
     </div>
     <div class="clearfix grpelem" id="u1235-5"><!-- content -->
      <p>35000 <span id="u1235-2">руб.</span></p>
     </div>
    </div>
    <div class="clearfix colelem" id="pu1311"><!-- group -->
     <div class="clearfix grpelem" id="u1311"><!-- column -->
      <div class="position_content" id="u1311_position_content">
       <div class="colelem" id="pu1312"><!-- inclusion -->
        <div id="u1312"><!-- simple frame --></div>
        <div class="clearfix" id="pu1313-4"><!-- group -->
         <div class="clearfix grpelem" id="u1313-4"><!-- content -->
          <p>Коттедж на 8 человек</p>
         </div>
        </div>
       </div>
       <div class="clearfix colelem" id="pu1321"><!-- group -->
        <div class="clip_frame grpelem" id="u1321"><!-- image -->
         <img class="block" id="u1321_img" src="images/wxpdtm-hflk-crop-u1321.jpg" alt="" width="338" height="389"/>
        </div>
        <div class="clearfix grpelem" id="ppu1320"><!-- column -->
         <div class="colelem" id="pu1320"><!-- inclusion -->
          <div id="u1320"><!-- simple frame --></div>
          <div class="clearfix" id="pu1314-6"><!-- column -->
           <div class="clearfix colelem" id="u1314-6"><!-- content -->
            <p>3 этажный коттедж на 8 человек, 4 двухместных номера</p>
            <p>Расчетный час: время заезда после 14:00, время выезда до 12:00.</p>
           </div>
           <div class="colelem" id="u1315"><!-- simple frame --></div>
           <div class="clearfix colelem" id="u1316-9"><!-- content -->
            <p id="u1316-3">В номере: <span id="u1316-2">банкетный зал на 20 человек,потолок 7м. DVD,Т.В., Караоке,WI-FI, кухня,со всей необходимой техникой: холодильник,свч-печь,стекло-керамическая плита, посуда, и. т. д. 7 комнат, 6 спален,2 санузла, душевые кабины.В номерах шкаф, туалетный столик, прикроватные тумбочки, дополнительный свет у изголовья кровати, WI-FI,спутниковое Т.В, с плоским экраном,балкон.</span></p>
            <p id="u1316-5">Номер может быть по желанию: DBL,TWN.</p>
            <p id="u1316-7">Дети до 10 лет бесплатно. (без отдельной кровати)</p>
           </div>
          </div>
         </div>
         <div class="clearfix colelem" id="pu1318-4"><!-- group -->
          <div class="clearfix grpelem" id="u1318-4"><!-- content -->
           <p>Цена:</p>
          </div>
          <a class="nonblock nontext rounded-corners clearfix grpelem" id="u1319-4" href="bronirovanie.html"><!-- content --><p>ЗАБРОНИРОВАТЬ</p></a>
         </div>
        </div>
       </div>
      </div>
     </div>
     <div class="clearfix grpelem" id="u1317-5"><!-- content -->
      <p>20000 <span id="u1317-2">руб.</span></p>
     </div>
    </div>
    <div class="clearfix colelem" id="pu1337"><!-- group -->
     <div class="clearfix grpelem" id="u1337"><!-- column -->
      <div class="position_content" id="u1337_position_content">
       <div class="colelem" id="pu1338"><!-- inclusion -->
        <div id="u1338"><!-- simple frame --></div>
        <div class="clearfix" id="pu1339-4"><!-- group -->
         <div class="clearfix grpelem" id="u1339-4"><!-- content -->
          <p>Дом на двоих</p>
         </div>
        </div>
       </div>
       <div class="clearfix colelem" id="pu1347"><!-- group -->
        <div class="clip_frame grpelem" id="u1347"><!-- image -->
         <img class="block" id="u1347_img" src="images/wxpdtm-hflk-crop-u1347.jpg" alt="" width="338" height="389"/>
        </div>
        <div class="clearfix grpelem" id="ppu1346"><!-- column -->
         <div class="colelem" id="pu1346"><!-- inclusion -->
          <div id="u1346"><!-- simple frame --></div>
          <div class="clearfix" id="pu1340-6"><!-- column -->
           <div class="clearfix colelem" id="u1340-6"><!-- content -->
            <p>Дом на двоих человек бассейном и баней, ночь с 23:00 до 11:00 = 4000 руб</p>
            <p>В ночное время баня с бассейном не работает.</p>
           </div>
           <div class="colelem" id="u1341"><!-- simple frame --></div>
           <div class="clearfix colelem" id="u1342-5"><!-- content -->
            <p>В номере:</p>
            <p>&nbsp;</p>
           </div>
          </div>
         </div>
         <div class="clearfix colelem" id="pu1344-4"><!-- group -->
          <div class="clearfix grpelem" id="u1344-4"><!-- content -->
           <p>Цена:</p>
          </div>
          <a class="nonblock nontext rounded-corners clearfix grpelem" id="u1345-4" href="bronirovanie.html"><!-- content --><p>ЗАБРОНИРОВАТЬ</p></a>
         </div>
        </div>
       </div>
      </div>
     </div>
     <div class="clearfix grpelem" id="u1343-5"><!-- content -->
      <p>10000 <span id="u1343-2">руб.</span></p>
     </div>
    </div>
    <div class="clearfix colelem" id="pu1350"><!-- group -->
     <div class="clearfix grpelem" id="u1350"><!-- column -->
      <div class="position_content" id="u1350_position_content">
       <div class="colelem" id="pu1351"><!-- inclusion -->
        <div id="u1351"><!-- simple frame --></div>
        <div class="clearfix" id="pu1352-4"><!-- group -->
         <div class="clearfix grpelem" id="u1352-4"><!-- content -->
          <p>Номер</p>
         </div>
        </div>
       </div>
       <div class="clearfix colelem" id="pu1360"><!-- group -->
        <div class="clip_frame grpelem" id="u1360"><!-- image -->
         <img class="block" id="u1360_img" src="images/wxpdtm-hflk-crop-u1360.jpg" alt="" width="338" height="389"/>
        </div>
        <div class="clearfix grpelem" id="ppu1359"><!-- column -->
         <div class="colelem" id="pu1359"><!-- inclusion -->
          <div id="u1359"><!-- simple frame --></div>
          <div class="clearfix" id="pu1353-4"><!-- column -->
           <div class="clearfix colelem" id="u1353-4"><!-- content -->
            <p>Номер</p>
           </div>
           <div class="colelem" id="u1354"><!-- simple frame --></div>
           <div class="clearfix colelem" id="u1355-13"><!-- content -->
            <p><span id="u1355">В номере:</span> санузел, душ, шкаф- купе, туалетный столик, прикроватные тумбочки, дополнительный свет у изголовья кровати, WI-FI,спутниковое Т.В, с плоским экраном,балкон.</p>
            <p>Номер может быть по желанию: DBL,TWN.</p>
            <p>Дети до 10 лет бесплатно. (без отдельной кровати)</p>
            <p>Доп. место - 2000 руб. (завтрак включен)</p>
            <p>на 1 этаже кафе</p>
           </div>
          </div>
         </div>
         <div class="clearfix colelem" id="pu1357-4"><!-- group -->
          <div class="clearfix grpelem" id="u1357-4"><!-- content -->
           <p>Цена:</p>
          </div>
          <a class="nonblock nontext rounded-corners clearfix grpelem" id="u1358-4" href="bronirovanie.html"><!-- content --><p>ЗАБРОНИРОВАТЬ</p></a>
         </div>
        </div>
       </div>
      </div>
     </div>
     <div class="clearfix grpelem" id="u1356-5"><!-- content -->
      <p>35000 <span id="u1356-2">руб.</span></p>
     </div>
    </div>
    <div class="verticalspacer"></div>
    <div class="clearfix colelem" id="pu1183"><!-- group -->
     <div class="browser_width grpelem" id="u1183-bw">
      <div id="u1183"><!-- simple frame --></div>
     </div>
     <div class="browser_width grpelem" id="u1198-bw">
      <div id="u1198"><!-- group -->
       <div class="clearfix" id="u1198_align_to_page">
        <div class="clearfix grpelem" id="pu1201"><!-- column -->
         <div class="clip_frame colelem" id="u1201"><!-- image -->
          <img class="block" id="u1201_img" src="images/phone.png" alt="" width="22" height="22"/>
         </div>
         <div class="clip_frame colelem" id="u1203"><!-- image -->
          <img class="block" id="u1203_img" src="images/point.png" alt="" width="14" height="23"/>
         </div>
        </div>
        <div class="clearfix grpelem" id="pu1206-4"><!-- column -->
         <div class="clearfix colelem" id="u1206-4"><!-- content -->
          <p>8 (3952) 73-40-98</p>
         </div>
         <div class="clearfix colelem" id="u1205-4"><!-- content -->
          <p>пос. Листвянка, ул. Чапаева, 98</p>
         </div>
        </div>
        <div class="clip_frame grpelem" id="u1199"><!-- image -->
         <img class="block" id="u1199_img" src="images/oplata.png" alt="" width="168" height="30"/>
        </div>
        <a class="nonblock nontext clip_frame grpelem" id="u1211" href="http://www.odnoklassniki.ru/profile/562900251596"><!-- image --><img class="block" id="u1211_img" src="images/ok.png" alt="" width="29" height="29"/></a>
        <a class="nonblock nontext clip_frame grpelem" id="u1209" href="http://www.facebook.com/profile.php?id=100009140466888"><!-- image --><img class="block" id="u1209_img" src="images/facebook.png" alt="" width="28" height="28"/></a>
        <a class="nonblock nontext clip_frame clearfix grpelem" id="u1207" href="http://vk.com/id232862672"><!-- image --><div id="u1207_clip"><img class="position_content" id="u1207_img" src="images/mezhdunarodny.png" alt="" width="30" height="30"/></div></a>
        <a class="nonblock nontext clip_frame grpelem" id="u1213" href="http://instagram.com/edem_baikal"><!-- image --><img class="block" id="u1213_img" src="images/insta.png" alt="" width="28" height="28"/></a>
       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
  <!-- JS includes -->
  <script type="text/javascript">
   if (document.location.protocol != 'https:') document.write('\x3Cscript src="http://musecdn.businesscatalyst.com/scripts/4.0/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
  <script type="text/javascript">
   window.jQuery || document.write('\x3Cscript src="scripts/jquery-1.8.3.min.js" type="text/javascript">\x3C/script>');
</script>
  <script src="scripts/museutils.js?275725342" type="text/javascript"></script>
  <script src="scripts/jquery.musepolyfill.bgsize.js?185257658" type="text/javascript"></script>
  <script src="scripts/jquery.watch.js?3999102769" type="text/javascript"></script>
  <!-- Other scripts -->
  <script type="text/javascript">
   $(document).ready(function() { try {
(function(){var a={},b=function(a){if(a.match(/^rgb/))return a=a.replace(/\s+/g,"").match(/([\d\,]+)/gi)[0].split(","),(parseInt(a[0])<<16)+(parseInt(a[1])<<8)+parseInt(a[2]);if(a.match(/^\#/))return parseInt(a.substr(1),16);return 0};(function(){$('link[type="text/css"]').each(function(){var b=($(this).attr("href")||"").match(/\/?css\/([\w\-]+\.css)\?(\d+)/);b&&b[1]&&b[2]&&(a[b[1]]=b[2])})})();(function(){$("body").append('<div class="version" style="display:none; width:1px; height:1px;"></div>');
for(var c=$(".version"),d=0;d<Muse.assets.required.length;){var f=Muse.assets.required[d],g=f.match(/([\w\-\.]+)\.(\w+)$/),k=g&&g[1]?g[1]:null,g=g&&g[2]?g[2]:null;switch(g.toLowerCase()){case "css":k=k.replace(/\W/gi,"_").replace(/^([^a-z])/gi,"_$1");c.addClass(k);var g=b(c.css("color")),h=b(c.css("background-color"));g!=0||h!=0?(Muse.assets.required.splice(d,1),"undefined"!=typeof a[f]&&(g!=a[f]>>>24||h!=(a[f]&16777215))&&Muse.assets.outOfDate.push(f)):d++;c.removeClass(k);break;case "js":k.match(/^jquery-[\d\.]+/gi)&&
typeof $!="undefined"?Muse.assets.required.splice(d,1):d++;break;default:throw Error("Unsupported file type: "+g);}}c.remove();if(Muse.assets.outOfDate.length||Muse.assets.required.length)c="Некоторые файлы на сервере могут отсутствовать или быть некорректными. Очистите кэш-память браузера и повторите попытку. Если проблему не удается устранить, свяжитесь с разработчиками сайта.",(d=location&&location.search&&location.search.match&&location.search.match(/muse_debug/gi))&&Muse.assets.outOfDate.length&&(c+="\nOut of date: "+Muse.assets.outOfDate.join(",")),d&&Muse.assets.required.length&&(c+="\nMissing: "+Muse.assets.required.join(",")),alert(c)})()})();
/* body */
Muse.Utils.transformMarkupToFixBrowserProblemsPreInit();/* body */
Muse.Utils.prepHyperlinks(true);/* body */
Muse.Utils.resizeHeight()/* resize height */
Muse.Utils.fullPage('#page');/* 100% height page */
Muse.Utils.showWidgetsWhenReady();/* body */
Muse.Utils.transformMarkupToFixBrowserProblems();/* body */
} catch(e) { if (e && 'function' == typeof e.notify) e.notify(); else Muse.Assert.fail('Error calling selector function:' + e); }});
</script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
   </body>
</html>
