@extends('layouts.mainlayout') @section('title', 'Отзывы о гостиничном комплексе Edem от гостей')
@section('meta')
    @include('meta.feedback-meta')
@stop
@section('beforemenu')
<div class="background-div">
    <div class="blackout-div">
        @stop @section('aftermenu')
        <div class="head-text">
            <h3>Отзывы</h3>
        </div>
    </div>
</div>
<div class="feedback">
    <div class="container">
        <h2>Количество отзывов: 3</h2>
        <div class="feedblock col-sm-12">
            <div class="nameblock col-sm-12">
                <p>
                    Валентина
                </p>
                <p id="date">
                    27.06.2016
                </p>
            </div>
            <div class="feedtext col-sm-12">
                <p>Отдыхали коллективом. Все очень понравилось, большое преимущество что мы были одни на территории.На следующее мероприятие компании поедем сюда.</p>
            </div>
        </div>
        <div class="feedblock col-sm-12">
            <div class="nameblock col-sm-12">
                <p>
                    Наталья
                </p>
                <p id="date">
                    12.02.2014
                </p>
            </div>
            <div class="feedtext col-sm-12">
                <p>Отдыхали с друзьями. Очень понравился коттедж и прилегающая территория. Всё сделано со вкусом и по-домашнему. Огромное спасибо!</p>
            </div>
        </div>
        <div class="feedblock col-sm-12">
            <div class="nameblock col-sm-12">
                <p>
                    Светлана
                </p>
                <p id="date">
                    02.12.2013
                </p>
            </div>
            <div class="feedtext col-sm-12">
                <p>Отличный дизайн коттеджа. Все очень понравилось. Приедем еще обязательно!</p>
            </div>
        </div>
        <div class="col-sm-12">
            <a href="http://booking.com/abf0be7515e1">ПОСМОТРЕТЬ ВСЕ ОТЗЫВЫ</a>
        </div>
    </div>
</div>
@stop