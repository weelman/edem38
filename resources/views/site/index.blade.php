@extends('layouts.mainlayout') @section('title', 'Большой и уютный гостиничный комплекс на берегу Байкала – Edem. Успейте забронировать домик прямо сейчас!')
@section('meta')
    @include('meta.index-meta')
@stop
@section('css')
<link rel="stylesheet" href="js-ui/jquery-ui.css">
<script src="js-ui/jquery-ui.js"></script>
@stop @section('beforemenu')
<div class="videoback">
    <video autoplay muted poster="/images/bg-head-min.jpg" id="bgvid" loop>
        <source src="/images/backgroundvideo.webm" type="video/webm">
        <source src="/images/backgroundvideo.mp4" type="video/mp4">
    </video>


<!--
<style>
    .navbar{margin-bottom: -70px;}
</style>
-->
@stop @section('aftermenu')

<div class="head-text" style="margin-top:17%;">
    <h2></h2>
    <div class="container">
        <div class="marsp col-sm-12">
            <form id="reservations_data" name="frmdata" class="form-inline" action="http://edem38ru-book.otelms.com/bookit/step1/3103" method="get">
                <div class="form-group">
                    <label for="exampleInputName" id="from">Дата заезда</label>
                    <div class="input-group">
                    <input class="form-control" id="ui-form-from" type="text" name="datein" value="">
                    <div class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="to">Дата отъезда</label>
                    <div class="input-group">
                    <input class="form-control" id="ui-form-to" type="text" name="dateout" value="">
                    <div class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></div>
                    </div>
                </div>
                <input type="hidden" name="countplaces" value="0">
                <button class="btn btn-default" type="submit" value="Проверить наличие мест">Проверить наличие мест</button>
            </form>
        </div>
    </div>
    <p></p>

    <a href="http://edem38ru-book.otelms.com/bookit/step1"></a>
</div>
</div>
<!--
      <video autoplay muted poster="/images/bg-head-min.jpg" id="bgvid" loop>
                <source src="/images/backgroundvideo.webm" type="video/webm">
                <source src="/images/backgroundvideo.mp4" type="video/mp4">
            </video>
-->
<div class="mainpage">
    <div class="container">
        <div class="main-blocks">
            <div class="content-block col-md-5 col-sm-12" id="main-img-1">
                <img src="/images/IMG_1347.JPG" alt="">
            </div>
            <div class="content-block col-md-7 col-sm-12" id="main-text-1">
                <center><img src="/images/wv1.png" alt=""></center>
                <h4>О НАС</h4>
                <p>Гостиничный комплекс “Edem” - изысканное место для ценителей не только высокого комфорта, но и любителей свежего воздуха. Комплекс удивляет очень красивым интерьером в современном стиле, который как нельзя лучше располагает к отдыху и релаксации.</p> 
                <p>Для того, чтобы расслабиться и получить еще большее удовольствие от отдыха в нашем комплексе, на территории расположена баня с площадкой для костра! 
                Гостиница «Edem» располагается в пешей доступности от озера Байкал в удивительном и чистом посёлке Листвянка.</p>
                <p>Добраться до нас очень просто! До посёлка часто ходят автобусы, маршрутный транспорт, а для тех, кто желает увидеть всю красоту природы ходит специальный чартерный корабль по реке Ангара. 
                Своим гостям мы предлагаем специальные тарифы и скидки на размещение, стараясь сделать проживание еще более выгодным для каждого гостя.</p>
            </div>
            <div class="content-block col-sm-12" id="main-img-2">
                <img src="/images/dsc_5178-pano.jpg" alt="">
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12" id="whereweare">
    <h3>ГДЕ НАС НАЙТИ?</h3>
</div>
<div class="col-sm-12">
    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=6_5NMnoZpBCAnT3470Dq5QBC9h3rpfiC&width=100%&height=400&lang=ru_RU&sourceType=constructor&scroll=true"></script>
</div>

<!--    <a class="dg-widget-link" href="http://2gis.ru/irkutsk/firm/1548641653311083/center/104.87847,51.862092/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Иркутска</a><div class="dg-widget-link"><a href="http://2gis.ru/irkutsk/firm/1548641653311083/photos/1548641653311083/center/104.87847,51.862092/zoom/17?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=photos">Фотографии компании</a></div><div class="dg-widget-link"><a href="http://2gis.ru/irkutsk/center/104.87847,51.862092/zoom/16/routeTab/rsType/bus/to/104.87847,51.862092╎Эдем, гостевой дом?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Эдем, гостевой дом</a></div><script charset="utf-8" src="http://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":'100%',"height":600,"borderColor":"#a3a3a3","pos":{"lat":51.862092,"lon":104.87847,"zoom":16},"opt":{"city":"irkutsk"},"org":[{"id":"1548641653311083"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>-->
<script type="text/javascript">
    $(function () {
        $("#ui-form-from").datepicker({
            dateFormat: 'dd/mm/yy'
        }, $.datepicker.regional['ru']);
        $("#ui-form-to").datepicker({
            dateFormat: 'dd/mm/yy'
        });

    });
</script>
<script>
    (function (factory) {
        if (typeof define === "function" && define.amd) {

            // AMD. Register as an anonymous module.
            define(["../widgets/datepicker"], factory);
        } else {

            // Browser globals
            factory(jQuery.datepicker);
        }
    }(function (datepicker) {

        datepicker.regional.ru = {
            closeText: "Закрыть",
            prevText: "&#x3C;Пред",
            nextText: "След&#x3E;",
            currentText: "Сегодня",
            monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
	"Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            monthNamesShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн",
	"Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
            dayNames: ["воскресенье", "понедельник", "вторник", "среда", "четверг", "пятница", "суббота"],
            dayNamesShort: ["вск", "пнд", "втр", "срд", "чтв", "птн", "сбт"],
            dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
            weekHeader: "Нед",
            dateFormat: "dd.mm.yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ""
        };
        datepicker.setDefaults(datepicker.regional.ru);

        return datepicker.regional.ru;

    }));
    
</script>
<script>
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	jQuery('video').css( 'display' , 'none' );
}

</script>
@stop