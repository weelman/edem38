@extends('layouts.mainlayout')
@section('meta')
    @include('meta.price-meta')
@stop
@section('title', 'Доступные цены на проживание в гостиничном комплексе Edem на берегу Байкала')
@section('beforemenu')
        <div class="background-div">
            <div class="blackout-div">
               @stop
                @section('aftermenu')
                <div class="head-text">
                    <h3>Цены</h3>
                </div>
            </div>
        </div>
    <div class="backfoblock">
        <div class="container no-padding">
            <div class="price-block col-lg-12 col-sm-12 col-md-12 col-xs-12" id="price-1">
                <h3>Мини-Отель</h3>
                <div class="line"></div>
                <div class="col-lg-12 col-sm-12 no-padding">
                    <div class="col-lg-5 col-sm-5">
                        <img src="images/price/mini-hotel.jpg" alt="">
                    </div>
                    <div class="col-lg-7 col-sm-7">
                        <div class="col-lg-12 price-text">
                            <p>3 этажный коттедж на 8 человек - 4 двухместных номера улучшенных с балконами.</p>
                            <div class="line"></div>
                            <p><strong>В номерах:&nbsp;</strong>санузел, душ, гель для душа, шампунь, шкаф- купе, туалетный столик, прикроватные тумбочки, дополнительный свет у изголовья кровати, WI-FI, спутниковое Т.В. с плоским экраном, балкон. Номер может быть по желанию: DBL,TWN. На 1 этаже кафе на 20 посадочных мест <br>Дети до 10 лет бесплатно (без отдельной кровати) <br>Доп. место - 1500 руб. (завтрак включен) <br><strong>Расчетный час: время заезда после 14:00, время выезда до 12:00.</strong></p>
                        </div>
                        <div class="col-lg-12 price-text-down">
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6 price-text-down-money"><p>Цена:&nbsp;<strong>5000</strong>&nbsp;руб/сут</p></div>
                            <div class="col-lg-6 col-xs-6 price-text-down-button">
                                <a href="http://edem38ru-book.otelms.com/bookit/step1">Забронировать</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="backfoblock">
        <div class="container no-padding">
            <div class="price-block col-lg-12 col-sm-12 col-md-12 col-xs-12" id="price-1">
                <h3>Коттедж на 20 человек</h3>
                <div class="line"></div>
                <div class="col-lg-12 col-sm-12 no-padding">
                    <div class="col-lg-5 col-sm-5">
                        <img src="images/price/cottage20.jpg" alt="">
                    </div>
                    <div class="col-lg-7 col-sm-7">
                        <div class="col-lg-12 price-text">
                            <p>Коттедж на 20 человек, включая три доп. места.</p>
                            <div class="line"></div>
                            <p><strong>В коттедже:&nbsp;</strong>банкетный зал на 20 человек, потолок 7м. DVD,Т.В., Караоке, WI-FI, кухня, со всей необходимой техникой: холодильник, свч-печь, стекло-керамическая плита, посуда, и.т.д. 7 комнат, 6 спален,2 санузла, душевые кабины. В номерах шкаф, туалетный столик, прикроватные тумбочки, дополнительный свет у изголовья кровати, WI-FI, спутниковое Т.В, с плоским экраном, балкон. Номер может быть по желанию: DBL,TWN. <br>Дети до 10 лет бесплатно (без отдельной кровати) <br><strong>Расчетный час: время заезда после 14:00, время выезда до 12:00.</strong></p>
                        </div>
                        <div class="col-lg-12 price-text-down">
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6 price-text-down-money">
                                <p>Цена:&nbsp;<strong>35000</strong>&nbsp;руб/сут</p>
                            </div>
                            <div class="col-lg-6 col-xs-6 price-text-down-button">
                                <a href="http://edem38ru-book.otelms.com/bookit/step1">Забронировать</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="backfoblock">
        <div class="container no-padding">
            <div class="price-block col-lg-12 col-sm-12 col-md-12 col-xs-12" id="price-1">
                <h3>Баня</h3>
                <div class="line"></div>
                <div class="col-lg-12 col-sm-12 no-padding">
                    <div class="col-lg-5 col-sm-5">
                        <img src="images/price/banya.jpg" alt="">
                    </div>
                    <div class="col-lg-7 col-sm-7">
                        <div class="col-lg-12 price-text">
                            <p>Баня с бассейном,  </p>
                            <div class="line"></div>
                            <p>Можно париться веником,  работает в нескольких температурных режимах (русская баня, сауна, хамам, финская сауна)</p>
                        </div>
                        <div class="col-lg-12 price-text-down">
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6 price-text-down-money">
                                <p>Цена:&nbsp;<strong>2000</strong>&nbsp;руб/час</p>
                            </div>
                            <div class="col-lg-6 col-xs-6 price-text-down-button">
                                <a href="http://edem38ru-book.otelms.com/bookit/step1">Забронировать</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="backfoblock">
        <div class="container no-padding">
            <div class="price-block col-lg-12 col-sm-12 col-md-12 col-xs-12" id="price-1">
                <h3>Дом на двоих</h3>
                <div class="line"></div>
                <div class="col-lg-12 col-sm-12 no-padding">
                    <div class="col-lg-5 col-sm-5">
                        <img src="images/price/lovehouse.jpg" alt="">
                    </div>
                    <div class="col-lg-7 col-sm-7">
                        <div class="col-lg-12 price-text">
                            <p>Дом на двоих с бассейном и баней</p>
                            <div class="line"></div>
                            <p>Общая площадь 100 м2, мини-бар (холодильник), спутниковое ТВ.,DVD, караоке, бассейн, парная, санузел, душ. <br>Дети до 10 лет бесплатно. (без отдельной кровати) <br>Доп. место - 1500 руб. (завтрак включен) <strong><br>Расчетный час: время заезда после 14:00, время выезда до 12:00.</strong></p>
                        </div>
                        <div class="col-lg-12 price-text-down">
                            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6 price-text-down-money"> <p>Цена:&nbsp;<strong>10000</strong>&nbsp;руб/сут</p>
                            </div>
                            <div class="col-lg-6 col-xs-6 price-text-down-button">
                                <a href="http://edem38ru-book.otelms.com/bookit/step1">Забронировать</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<div class="container no-padding" style="text-align:center">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false">
              Дополнительная информация
            </a>
          </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
          <div class="panel-body">
            <a href="/doc/summer-menu.docx">Летнее меню</a>*
            <a href="/doc/breakfast.docx">Завтраки</a>*
            <a href="/doc/business-lunch.docx">Бизнес ланч</a>*
            <a href="/doc/costing.docx">Расчет стоимости на проживание</a>*
            <a href="/doc/rules.doc">Правила проживания Эдем</a>*
            <a href="/doc/attachment.doc">Приложение 1</a>*
          </div>
        </div>
      </div>
    </div>
</div>  
    
    
<script>
    jQuery('#headingTwo').hover(
        function(){
            jQuery('#headingTwo').css('opacity', '0.8');
        }, function(){
            jQuery('#headingTwo').css('opacity', '1');
        }
    );
</script> 
    
   @stop

