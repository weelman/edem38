<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="yandex-verification" content="1d02b01033f9bf6f" />
    <meta name='wmail-verification' content='402ef4bcbdf719506be056145fa032ed' />
    @yield('meta')
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title')</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/redesign.css" rel="stylesheet" />
    <link href="css/blocks.css" rel="stylesheet" /> 
    <link rel="shortcut icon" href="/images/favicon.gif" type="image/x-icon">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js"></script>
    @yield('scripts')
    
    @yield('css')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>

<body>

{!! HTML::macro('activeState', function($url)
        {
            return ($url ==  Request::url()) ? 'active' : '';
            //return Request::is($url) ? 'active' : '';
        });!!}

    @yield('beforemenu')
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" style="background-color: rgba(8, 133, 187, 1.7);">
                    <span class="sr-only">Меню</span>
                    <p style="margin:-2px">Меню</p>
<!--
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
-->
                </button>
                <a href="{{ URL::route('index')}}">
                    <img class="logo-img" src="images/logo.png" alt="">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a class="{{HTML::activeState(URL::route('price'))}}" href="{{ URL::route('price')}}">ЦЕНЫ</a></li>
                    <li><a href="http://edem38ru-book.otelms.com/bookit/step1">БРОНИРОВАНИЕ</a></li>
                    <li><a class="{{HTML::activeState(URL::route('about'))}}" href="{{ URL::route('about')}}">О НАС</a></li>
                    <li><a class="{{HTML::activeState(URL::route('gallery'))}}" href="{{ URL::route('gallery')}}">ГАЛЕРЕЯ</a></li>
                    <li><a class="{{HTML::activeState(URL::route('visa'))}}" href="{{ URL::route('visa')}}">ВИЗА</a></li>
                    <li><a class="{{HTML::activeState(URL::route('contacts'))}}" href="{{ URL::route('contacts')}}">КОНТАКТЫ</a></li>
                    <li><a class="{{HTML::activeState(URL::route('feedback'))}}" href="{{ URL::route('feedback')}}">ОТЗЫВЫ</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li style="height: 23px;"><a href="#">LANGUAGE</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    @yield('aftermenu')

    <div class="footer-bot col-sm-12">
        <div class="container">
            <div class="col-sm-4 col-xs-12 footer-contacts">
                <p><img src="images/phone.png"> 8 (3952) 73-40-98</p>
                <p>&nbsp;<img src="images/point.png" alt="">&nbsp;&nbsp;пос. Листвянка, ул. Чапаева, 104а</p>
            </div>
            <div class="col-sm-4 col-md-3 col-xs-12 footer-pay">
                <img src="images/oplata.png" alt="">
            </div>
            <div class="col-md-offset-1 col-sm-3 col-xs-12">
                <p class="footer-soc">
                    <a href="http://www.odnoklassniki.ru/profile/562900251596"><img class="social" src="images/ok.png" alt=""></a>
                    <a href="http://www.facebook.com/profile.php?id=100009140466888">
                        <img class="social" src="images/facebook.png" alt=""></a>
                    <a href="http://vk.com/id232862672">
                        <img class="social" src="images/mezhdunarodny.png" alt=""></a>
                    <a href="http://instagram.com/edem_baikal">
                        <img class="social" src="images/insta.png" alt=""></a>
                </p>
            </div>
        </div>
    </div>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/formenu.js"></script>
    <!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter38768485 = new Ya.Metrika({
                    id:38768485,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/38768485" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "PR-CY",
  "url" : "http://pr-cy.ru",
  "sameAs" : [
    "http://vk.com/id232862672",
    "https://www.facebook.com/profile.php?id=100009140466888",
    "https://www.instagram.com/edem_baikal",
    "https://ok.ru/profile/562900251596"
  ]
}
</script>
</body>

</html>
