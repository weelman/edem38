jQuery(document).ready(function() {
var h = jQuery(window).height();
var w = jQuery(window).width();
if (w > 991) {
jQuery('.background-about').height(h);
jQuery(window).resize(function() {
jQuery('.background-about').height(h);
});
}
});
jQuery(document).ready(function(jQuery){
	var introSection = jQuery('.background-about img'),
		introSectionHeight = introSection.height(),
		//change scaleSpeed if you want to change the speed of the scale effect
		scaleSpeed = 0.3,
		//change opacitySpeed if you want to change the speed of opacity reduction effect
		opacitySpeed = 1; 
	
	//update this vale if you change this breakpoint in the style.css file (or _layout.scss if you use SASS)
	var MQ = 1170;

	triggerAnimation();
	jQuery(window).on('resize', function(){
		triggerAnimation();
	});

	//bind the scale event to window scroll if window width > jQueryMQ (unbind it otherwise)
	function triggerAnimation(){
		if(jQuery(window).width()>= MQ) {
			jQuery(window).on('scroll', function(){
				//The window.requestAnimationFrame() method tells the browser that you wish to perform an animation- the browser can optimize it so animations will be smoother
				window.requestAnimationFrame(animateIntro);
			});
		} else {
			jQuery(window).off('scroll');
		}
	}
	//assign a scale transformation to the #cd-background element and reduce its opacity
	function animateIntro () {
		var scrollPercentage = (jQuery(window).scrollTop()/introSectionHeight).toFixed(5),
			scaleValue = 1 - scrollPercentage*scaleSpeed;
		//check if the #cd-background is still visible
		if( jQuery(window).scrollTop() < introSectionHeight) {
			introSection.css({
			    '-moz-transform': 'scale(' + scaleValue + ') translateZ(0)',
			    '-webkit-transform': 'scale(' + scaleValue + ') translateZ(0)',
				'-ms-transform': 'scale(' + scaleValue + ') translateZ(0)',
				'-o-transform': 'scale(' + scaleValue + ') translateZ(0)',
				'transform': 'scale(' + scaleValue + ') translateZ(0)',
				'opacity': 1 - scrollPercentage*opacitySpeed
			});
		}
	}	
});
jQuery('.before').click(function() {
	jQuery('.overflow').css( 'display' , 'block' );
	jQuery('.before').css( 'overflow' , 'visible' );
});
jQuery('.overflow').click(function() {
	jQuery('.overflow').css( 'display' , 'none' );
	jQuery('.before').css( 'overflow' , 'hidden' );
});
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	jQuery('video').css( 'display' , 'none' );
}