<?php

use Illuminate\Database\Seeder;

use App\Contact;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contact::create([
			'name'=>'Наш адрес',
            'body'=>'пос. Листвянка, ул. Чапаева, 98',
		]);
        Contact::create([
			'name'=>'Наш телефон',
            'body'=>'8 (3952) 73-40-98',
		]);
        Contact::create([
			'name'=>'E-mail',
            'body'=>'edem38@mail.ru',
		]);
    }
}
