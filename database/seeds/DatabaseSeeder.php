<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        
        DB::table('contacts')->delete();
        DB::table('users')->delete();
        
        $this->call(ContactSeeder::class);
        $this->call(UserSeeder::class);
    }
}
