<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'index', function () {
    return view('site.index');
}]);
Route::get('about', ['as' => 'about', function () {
    return view('site.about');
}]);
Route::get('booking', ['as' => 'booking', function () {
    return view('site.booking');
}]);
Route::get('contacts', ['as' => 'contacts', 'uses' => 'ContactsController@onsite']);
Route::get('gallery', ['as' => 'gallery', function () {
    return view('site.gallery');
}]);
Route::get('price', ['as' => 'price', function () {
    return view('site.price');
}]);
Route::get('price2', ['as' => 'price2', function () {
    return view('site.price2');
}]);
Route::get('feedback', ['as' => 'feedback', function () {
    return view('site.feedback');
}]);
Route::get('visa', ['as' => 'visa', function () {
    return view('site.visa');
}]);
Route::get('test', ['as' => 'test', function () {
    return view('site.test');
}]);
Route::group(['prefix'=>'lk', 'middleware' => 'auth'], 
function () {
    Route::get('editcontacts', ['as' => 'editcontacts', function () {
        
    }]);
});

//редиректы
Route::get('o-nas.htm', ['as' => 'o-nas.htm', function () {
    return redirect()->route('about');
}]);
Route::get('gallery.htm', ['as' => 'gallery.htm', function () {
    return redirect()->route('gallery');
}]);
Route::get('photo.htm', ['as' => 'photo.htm', function () {
    return redirect()->route('gallery');
}]);
Route::get('coin.htm', ['as' => 'coin.htm', function () {
    return redirect()->route('price');
}]);
Route::get('contact.htm', ['as' => 'contact.htm', function () {
    return redirect()->route('contacts');
}]);

Route::get('elitnyj-apartament-otel-v-listvjanke.htm', ['as' => 'elitnyj-apartament-otel-v-listvjanke.htm', function () {
    return redirect()->route('price');
}]);



Route::auth();

Route::get('/home', 'HomeController@index');

