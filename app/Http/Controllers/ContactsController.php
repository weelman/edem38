<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;

use App\Http\Requests;

class ContactsController extends Controller
{
    public function onsite() 
    {
        $contact = Contact::all();
        return view('site.contacts')->with('contact', $contact);
    }
}
